% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/performance_metrics.R
\name{ustat2b}
\alias{ustat2b}
\title{Calculate Theil's U-statistic. Variant.}
\usage{
ustat2b(expect, obs, ...)
}
\arguments{
\item{expect}{A number vector, representing the expected or forecasted
values.}

\item{obs}{A number vector, representing the observed values.}
}
\description{
Calculate Theil's U-statistic. Variant.
}
\details{
This version expects the obs vector to have one data point earlier
than expect vector, so like last step forecast can work properly. First
element of expect would be an NA. Theil's U-statistic (Thiel, H 1966. Applied
economic forecasting).
}
