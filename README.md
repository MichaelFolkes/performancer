# R package: performancer 
A suite of R functions for performance analyses including cross-validation, jackknifing, then calculation of performance metrics. Running the following two lines in R will install the `remotes` package from CRAN, then install `performancer` from gitlab. Not all functions have been fully tested. 

If you are using Ubuntu/Linux, for a successful install of devtools, you may need to run this line in BASH:

`sudo apt-get install libcurl4-openssl-dev libssl-dev`

Then in R:
    
```{r} 
install.packages("remotes") 
remotes::install_git("https://gitlab.com/michaelfolkes/performancer") 
```

Once the package is installed, if using Rstudio, you will find some basic information in the user guides area of the package documentation. Giving credit is nice. You can obtain citation details for this R package using the command: 
`citation('performancer')`.
